

import React, {useRef, Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ReactBnbGallery from 'react-bnb-gallery'
import { logoutUser } from "../../actions/authActions";
import Webcam from "react-webcam";
import axios from 'axios';

class Dashboard extends Component {
  
  constructor(props) {
    super(props);

    this.onFileChange = this.onFileChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.capture = this.capture.bind(this)
    this.next = this.next.bind(this)
    this.state = { galleryOpened: false };
    this.toggleGallery = this.toggleGallery.bind(this);
    this.state = {
      email: "",
        profileImg: '',
        data:[]
    }
}
setRef = webcam => {
  this.webcam = webcam;
};
onFileChange(e) {
    this.setState({ profileImg: e.target.files[0] })
}

async capture(imgSrc) {
  setInterval(async () => {
  const imageSrc = this.webcam.getScreenshot();
 
  const { user } = this.props.auth;
 const userData = JSON.stringify({
      email: user.id,
      image:imageSrc
    });

    const response = await axios.post('http://localhost:5005/api/users/user-profile',
    {   email: user.id,
      image:imageSrc}, {
    }).then(res => {
        console.log(res)
        this.next();
    })

  
}  , 30000);
}
toggleGallery() {
  this.setState(prevState => ({
    galleryOpened: !prevState.galleryOpened
  }));
}
async next() {
  console.log("Refresh")
  const response = await axios.get('http://localhost:5005/api/users/allImages/images',
 {
  }).then(res => {
      // console.log(res.data)
      this.makeData(res.data);
  })

}

makeData = values => {
  var data1 = [];
 values.forEach(element => {

// let str =element.image.split(',')[1]
// console.log('str',str)
    let obj = {
        photo:element.image
  
    }

      data1.push(obj);
    
  });

  this.setState({data: data1})


};


async onSubmit(e) {
  
  e.preventDefault()

  const { user } = this.props.auth;
  const {img}=this.state.profileImg

  const userData = JSON.stringify({
      email: user.id,
      image:this.state.profileImg
    });

    // console.log('userimage',img)
    console.log('DATA',this.state.profileImg)
      const response = await axios.post('http://localhost:5004/api/users/user-profile',
      {   email: user.id,
        image:this.state.profileImg.name}, {
      }).then(res => {
          console.log(res)
          
      })
}

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { user } = this.props.auth;
    const data1 = this.state.data;
    
    console.log(user)
    return (
      <div style={{ height: "50vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="landing-copy col s12 center-align">
            <h4>
              <b>Hey there,</b> {user.name.split(" ")[0]}
              {user.email}
              <p className="flow-text grey-text text-darken-1">
                You are logged into a full-stack{" "}
                <span style={{ fontFamily: "monospace" }}>AYN TEST APP</span> app 👏
              </p>
            </h4>
          
            <div className="text-center">
            
            <Webcam
                audio={false}
                height={500}
                ref={this.setRef}
                screenshotFormat="image/jpeg"
                width={600} >
         
                </Webcam>
            <div>
            <button  
             style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              
              className="btn btn-large waves-effect waves-light hoverable blue accent-3" onClick={this.capture}>START</button>

<>   </>
            <button      style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              
              className="btn btn-large waves-effect waves-light hoverable blue accent-3" onClick={this.next}>Refresh Gallery</button>
            </div>
        </div>
       

        <br></br>
        <div>
    {data1.map((item, index) => (
   <img src={`${item.photo}`} width="100" height="100" />
    ))}

   </div>




            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);

const videoConstraints = {
  width: 680,
  height: 480,
  facingMode: "user"
};