const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const imageUploadSchema = new Schema({

  email: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },

  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = imageUpload = mongoose.model("imageUpload", imageUploadSchema);
